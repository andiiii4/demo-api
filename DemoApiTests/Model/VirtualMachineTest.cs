﻿using DemoApi.Model;

namespace DemoApiTests.Model
{
    public class VirtualMachineTest
    {
        [Fact]
        public void VmIsPAtchable_TodayIsPatchDayAndVmIsRunning_ReturnTrue()
        {
            var virtualMachine = new VirtualMachine() { Status = "Running" };

            Assert.True(virtualMachine.VmIsPatchable(DateTime.Now.Day));
        }

        [Fact]
        public void VmIsPAtchable_TodayIsNotPatchDayAndVmIsRunning_ReturnFalse()
        {
            var virtualMachine = new VirtualMachine() { Status = "Running" };

            Assert.False(virtualMachine.VmIsPatchable(DateTime.Now.Day + 1));
        }

        [Fact]
        public void VmIsPAtchable_TodayIsPatchDayAndVmIsNotRunning_ReturnFalse()
        {
            var virtualMachine = new VirtualMachine() { Status = "Stopped" };

            Assert.False(virtualMachine.VmIsPatchable(DateTime.Now.Day));
        }

        [Fact]
        public void VmIsPAtchable_TodayIsPatchDayStatusIsEmptyString_ReturnFalse()
        {
            var virtualMachine = new VirtualMachine() { Status = "" };

            Assert.False(virtualMachine.VmIsPatchable(DateTime.Now.Day));
        }

        [Fact]
        public void VmIsPAtchable_TodayIsPatchDayStatusIsNull_ReturnFalse()
        {
            var virtualMachine = new VirtualMachine() { Status = null };

            Assert.False(virtualMachine.VmIsPatchable(DateTime.Now.Day));
        }
    }
}
using DemoApi.Logic;
using DemoApi.Model;

namespace DemoApiTests.Logic
{
    public class VirtualMachineAccessorTest
    {
        [Fact]
        public void CountParentVmsTest_SimpleCorrectInput_ReturnCorrectCountForAll()
        {
            var virtualMachines = new List<VirtualMachine>() {
        new VirtualMachine() {
          Id = 1,
          ParentId = 3
        },
        new VirtualMachine() {
          Id = 2,
          ParentId = 1
        },
        new VirtualMachine(){
          Id = 3,
          ParentId = null
        }
      };
            VirtualMachineAccessor.CountParentVms(virtualMachines);

            Assert.Equal(1, virtualMachines.Where(x => x.Id == 1).First().ParentVmCount);
            Assert.Equal(2, virtualMachines.Where(x => x.Id == 2).First().ParentVmCount);
            Assert.Equal(0, virtualMachines.Where(x => x.Id == 3).First().ParentVmCount);
        }

        [Fact]
        public void CountParentVmsTest_InputCircularParentReferences_ReturnCount3ForEach()
        {
            var virtualMachines = new List<VirtualMachine>() {
        new VirtualMachine() {
          Id = 4,
          ParentId = 6
        },
        new VirtualMachine() {
          Id = 5,
          ParentId = 4
        },
        new VirtualMachine(){
          Id = 6,
          ParentId = 5
        }
      };
            VirtualMachineAccessor.CountParentVms(virtualMachines);

            Assert.Equal(2, virtualMachines.Where(x => x.Id == 4).First().ParentVmCount);
            Assert.Equal(2, virtualMachines.Where(x => x.Id == 5).First().ParentVmCount);
            Assert.Equal(2, virtualMachines.Where(x => x.Id == 6).First().ParentVmCount);
        }

        [Fact]
        public void CountParentVmsTest_InputContainingNullObject_ReturnCorrectCountForTheOthers()
        {
            var virtualMachines = new List<VirtualMachine>() {
        new VirtualMachine() {
          Id = 1,
          ParentId = 3
        },
        new VirtualMachine() {
          Id = 2,
          ParentId = 1
        },
        new VirtualMachine(){
          Id = 3,
          ParentId = null
        },
        null
      };
            VirtualMachineAccessor.CountParentVms(virtualMachines);

            Assert.Equal(1, virtualMachines.Where(x => x.Id == 1).First().ParentVmCount);
            Assert.Equal(2, virtualMachines.Where(x => x.Id == 2).First().ParentVmCount);
            Assert.Equal(0, virtualMachines.Where(x => x.Id == 3).First().ParentVmCount);
        }

        [Fact]
        public void CountParentVmsTest_ComplexInputWithCircularParentReferencesAndNullObject_ReturnCorrectCountForAllNonNull()
        {
            var virtualMachines = new List<VirtualMachine>() {
        new VirtualMachine() {
          Id = 1,
          ParentId = 2
        },
        new VirtualMachine() {
          Id = 2,
          ParentId = null
        },
        new VirtualMachine(){
          Id = 3,
          ParentId = 1
        },new VirtualMachine() {
          Id = 4,
          ParentId = 6
        },
        new VirtualMachine() {
          Id = 5,
          ParentId = 4
        },
        new VirtualMachine() {
          Id = 6,
          ParentId = 5
        },
      };
            VirtualMachineAccessor.CountParentVms(virtualMachines);

            Assert.Equal(1, virtualMachines.Where(x => x.Id == 1).First().ParentVmCount);
            Assert.Equal(0, virtualMachines.Where(x => x.Id == 2).First().ParentVmCount);
            Assert.Equal(2, virtualMachines.Where(x => x.Id == 3).First().ParentVmCount);
            Assert.Equal(2, virtualMachines.Where(x => x.Id == 4).First().ParentVmCount);
            Assert.Equal(2, virtualMachines.Where(x => x.Id == 5).First().ParentVmCount);
            Assert.Equal(2, virtualMachines.Where(x => x.Id == 6).First().ParentVmCount);
        }

        [Fact]
        public void CountParentVmsTest_EmptyListInput_NoExceptions()
        {
            var virtualMachines = new List<VirtualMachine>();
            VirtualMachineAccessor.CountParentVms(virtualMachines);

            Assert.True(true);
        }

        [Fact]
        public void CountParentVmsTestNullInput_NoExceptions()
        {
            VirtualMachineAccessor.CountParentVms(null);

            Assert.True(true);
        }

        [Fact]
        public void GetVirtualMachinesWithStatusTest_CorrectListAndCorrectStatus_ReturnFoundVms()
        {
            var virtualMachines = new List<VirtualMachine>() {
        new VirtualMachine() {
          Id = 1,
          ParentId = 2,
          Status = "Running"
        },
        new VirtualMachine() {
          Id = 2,
          ParentId = null,
          Status = "Stopped"
        },
        new VirtualMachine(){
          Id = 3,
          ParentId = 1,
          Status = "Running"
        },new VirtualMachine() {
          Id = 4,
          ParentId = 6,
          Status = ""
        },
        new VirtualMachine() {
          Id = 5,
          ParentId = 4,
          Status = null
        },
        new VirtualMachine() {
          Id = 6,
          ParentId = 5,
          Status = "Running"
        }
      };

            var runningVms = VirtualMachineAccessor.GetVirtualMachinesWithStatus("running", virtualMachines);

            Assert.Equal(3, runningVms.Count);
        }

        [Fact]
        public void GetVirtualMachinesWithStatusTest_EmptyStatus_ReturnEmptyList()
        {
            var virtualMachines = new List<VirtualMachine>() {
        new VirtualMachine() {
          Id = 1,
          ParentId = 2,
          Status = "Running"
        }
      };

            var runningVms = VirtualMachineAccessor.GetVirtualMachinesWithStatus("", virtualMachines);

            Assert.Empty(runningVms);
        }

        [Fact]
        public void GetVirtualMachinesWithStatusTest_NullStatus_ReturnEmptyList()
        {
            var virtualMachines = new List<VirtualMachine>() {
        new VirtualMachine() {
          Id = 1,
          ParentId = 2,
          Status = "Running"
        }
      };

            var runningVms = VirtualMachineAccessor.GetVirtualMachinesWithStatus(null, virtualMachines);

            Assert.Empty(runningVms);
        }

        [Fact]
        public void GetVirtualMachinesWithStatusTest_EmptyList_ReturnEmptyList()
        {
            var virtualMachines = new List<VirtualMachine>();

            var runningVms = VirtualMachineAccessor.GetVirtualMachinesWithStatus("running", virtualMachines);

            Assert.Empty(runningVms);
        }

        [Fact]
        public void GetVirtualMachinesWithStatusTest_NullInput_ReturnEmptyList()
        {
            var runningVms = VirtualMachineAccessor.GetVirtualMachinesWithStatus("running", null);

            Assert.Empty(runningVms);
        }
    }
}
﻿using DemoApi.Controllers;
using DemoApi.Model;
using System.Net.Http.Headers;
using System.Text.Json;

namespace DemoApi.Logic {
  public class VirtualMachineAccessor {
    private readonly ILogger<DemoApiController> _logger;
    public string Uri { get; }
    public string BearerToken { get; }

    public VirtualMachineAccessor(ILogger<DemoApiController> logger, string uri, string bearerToken) {
      _logger = logger;
      Uri = uri;
      BearerToken = bearerToken;
    }

    public List<VirtualMachine> GetAll() {
      using (var client = new HttpClient()) {
        client.BaseAddress = new Uri(Uri);
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", BearerToken);
        client.DefaultRequestHeaders.Accept.Clear();
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        var responseTask = client.GetAsync("virtualmachines");
        responseTask.Wait();

        var result = responseTask.Result;
        if (result.IsSuccessStatusCode) {
          var readTask = result.Content.ReadAsStringAsync();
          readTask.Wait();

          var virtualMachinesJson = readTask.Result;
          var virtualMachines = JsonSerializer.Deserialize<List<VirtualMachine>>(virtualMachinesJson);
          if (virtualMachines is not null) {
            CountParentVms(virtualMachines);
            _logger.LogInformation("Successfully querried all virtual machines");
            return virtualMachines;
          }
        }
      }
      return new List<VirtualMachine>();
    }

    public static void CountParentVms(List<VirtualMachine> virtualMachines) {
      if (virtualMachines is null) {
        return;
      }
      var virtualMachinesDict = new Dictionary<int, VirtualMachine>();
      foreach (var virtualMachine in virtualMachines) {
        if (virtualMachine is not null) {
          virtualMachinesDict.Add(virtualMachine.Id, virtualMachine);
        }
      }
      foreach (var virtualMachine in virtualMachines) {
        if (virtualMachine is not null && virtualMachine.ParentVmCount == 0 && virtualMachine.ParentId != 0) {
          virtualMachine.ParentVmCount = CountParentVms(virtualMachine, virtualMachinesDict);
        }
      }
    }

    private static int CountParentVms(VirtualMachine virtualMachine, Dictionary<int, VirtualMachine> virtualMachinesDict) {
      var result = 0;
      if (virtualMachine.ParentId is null) {
        return result;
      }
      virtualMachinesDict.Remove(virtualMachine.Id);
      if (virtualMachinesDict.TryGetValue((int)virtualMachine.ParentId, out var parentVirtualMachine)) {
        result += 1 + CountParentVms(parentVirtualMachine, virtualMachinesDict);
      }
      virtualMachinesDict.Add(virtualMachine.Id, virtualMachine);
      return result;
    }

    public List<VirtualMachine> GetByStatus(string status) {
      return GetVirtualMachinesWithStatus(status, GetAll());
    }

    public static List<VirtualMachine> GetVirtualMachinesWithStatus(string status, List<VirtualMachine> virtualMachines) {
      if (status is null || virtualMachines is null) {
        return new List<VirtualMachine>();
      }
      return virtualMachines.Where(
        vm => vm is not null &&
        vm.Status is not null &&
        vm.Status.ToLower().Equals(status.ToLower())
      ).ToList();
    }
  }
}

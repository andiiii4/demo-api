﻿using System.Text.Json.Serialization;

namespace DemoApi.Model {
  public class VirtualMachine {
    [JsonPropertyName("id")]
    public int Id { get; set; }

    [JsonPropertyName("isStartable")]
    public bool? IsStartable { get; set; }

    [JsonPropertyName("location")]
    public string? Location { get; set; }

    [JsonPropertyName("owner")]
    public string? Owner { get; set; }

    [JsonPropertyName("createdBy")]
    public string? CreatedBy { get; set; }

    [JsonPropertyName("name")]
    public string? Name { get; set; }

    [JsonPropertyName("status")]
    public string? Status { get; set; }

    [JsonPropertyName("tags")]
    public List<string>? tags { get; set; }

    [JsonPropertyName("cpu")]
    public int Cpu { get; set; }

    [JsonPropertyName("ram")]
    public long Ram { get; set; }

    [JsonPropertyName("createdAt")]
    public string? CreatedAt { get; set; }

    [JsonPropertyName("parentId")]
    public int? ParentId { get; set; }

    [JsonPropertyName("isPatchable")]
    public bool IsPatchable => VmIsPatchable(3);

    [JsonPropertyName("parentVmCount")]
    public int ParentVmCount { get; set; }

    public bool VmIsPatchable(int patchDay) {
      return Status is not null && DateTime.Now.Day.Equals(patchDay) && Status.Equals("Running");
    }
  }
}

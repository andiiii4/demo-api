﻿using DemoApi.Logic;
using DemoApi.Model;
using Microsoft.AspNetCore.Mvc;

namespace DemoApi.Controllers {
  [ApiController]
  [Route("[controller]")]
  public class DemoApiController : ControllerBase {
    private readonly ILogger<DemoApiController> _logger;

    private readonly VirtualMachineAccessor _vmAccessor;

    public DemoApiController(ILogger<DemoApiController> logger) {
      _logger = logger;
      _vmAccessor = new VirtualMachineAccessor(_logger,
        "https://plattformapi.herokuapp.com/",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c");
      _logger.LogInformation("Application up and running...");
    }

    [HttpGet("virtualmachines")]
    public List<VirtualMachine> Get() {
      return _vmAccessor.GetAll();
    }

    [HttpGet("virtualmachines/{status}")]
    public List<VirtualMachine> GetByStatus(string status) {
      return _vmAccessor.GetByStatus(status);
    }
  }
}

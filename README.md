Inventx Praxisaufgabe 

Ausgangslage 

Es existiert eine öffentliche JSON REST-API, welche uns eine Beispielliste von virtuellen Maschinen in unserer Cloud-Plattform liefert (auf "/virtualmachines"). Nachfolgend soll diese als "Plattform-API" bezeichnet werden. 

Ziel der Aufgabe ist es eine eigene JSON API mittels Java (Technologien frei wählbar) zu schreiben, welche ihrerseits einen eigenen Endpunkt "/virtualmachines" anbietet. Nachfolgend soll diese als "Demo-API" bezeichnet werden. 

Zur Unterstützung wurde für die Demo-API ein Code Repository unter  https://gitlab.com/-/ide/project/stampwil1/demo-api/ erstellt, auf dem du deinen Code einchecken kannst, wenn du damit fertig bist. 




Aufgabenstellung 

Erstelle in der Demo-API einen neuen Endpunkt "/virtualmachines". Dieser soll die Plattform-API aufrufen (GET auf /virtualmachines), anschliessend die erhaltenen VMs um das Feld "isPatchable" erweitern und die erweiterten VMs dem Aufrufer der Demo-API als JSON zurückgeben. 

Das Feld "isPatchable" soll dabei true sein, wenn laut aktuellem Datum (lokale Systemzeit der Demo-API) der dritte Tag im Monat ist und das Feld "status" der VM auf "Running" ist. Ansonsten soll "isPatchable" false sein. 

Die Businesslogik soll wo du es für nötig erachtest mit Unittests versehen werden. Wir sollen erkennen, wie du generell Businesslogik mit UnitTests prüfst und welche Fälle dir dabei wichtig/ prüfenswert erscheinen 

Die Änderungen am Code sollen als Merge-Request auf dem Gitlab-Repo erfasst werden. 




Plattform-API 

REST Endpunkt: https://plattformapi.herokuapp.com/virtualmachines

Bearer Token für Authentifizierung an der Plattform-API:
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c 

  

Zielsetzung der Aufgabe 

Es geht darum zu sehen, wie du ein solches Problem löst und wie du generell arbeitest und vorgehst - was deine Überlegungen sind. Es ist nebensächlich, dass alle fachlichen Anforderungen erfüllt sind. Auch sollen zum Beispiel nicht alle Fälle durch UnitTests abgedeckt sein, sondern wir sollen erkennen, wie du eine hohe Abdeckung inklusive EdgeCases erreichst. 

Der Zeitaufwand sollte sich im Rahmen von 1-2 Stunden bewegen. Falls ein paar Details dann noch fehlen, ist das nicht schlimm. Und halte die Lösung simpel. Falls ein paar Details dann noch fehlen ist es auch in Ordnung, wenn du uns morgen einfach genau erklären kannst, was du sonst noch gemacht hättest. 
